def fizz_buzz(n)
  if (n % 15).zero?
    'FizzBuzz'
  elsif (n % 3).zero? # n % 3 == 0
    'Fizz'
  elsif (n % 5).zero?
    'Buzz'
  else
    n.to_s
  end
end
